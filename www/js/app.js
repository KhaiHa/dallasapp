// Ionic Starter App
(function() {
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('DallasApp', ['ionic', 'angularMoment', 'ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/home');

  $stateProvider

  // HOME STATES AND NESTED VIEWS ========================================
    .state('home', {
      url: '/home',
      templateUrl: 'templates/homepage.html'
    })
    // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
    .state('comments', {
      url: '/comments/:id',
      templateUrl: 'templates/comments.html'
    })

});

app.controller('DallasCtrl', function($http, $scope) {
    $scope.stories=[];
    $scope.stories = [{
    title: "Lost Dog: Whose Doge is this anyway?",
    thumbnail : "http://vignette1.wikia.nocookie.net/sanicsource/images/9/97/Doge.jpg/revision/latest?cb=20160112233015",
    ups: "49",
    id: "abc0",
    created_utc: 1479583399,
    comments: ["DogeLover9: Oh my god he is so cute!", "Observant1: Yea i saw him yesterday as well", "Janet6: caught my son feeding him the other night", "MickaMicka: Used to own one just like him "]
    },
    {
    "title": "caught this guy red handed. Called the cops!",
    thumbnail : "http://inserbia.info/news/wp-content/uploads/2013/06/carthief.jpg",
    ups: "36",
    id: "abc1",
    created_utc: 1479583399,
    comments: ["comment1", "comment2", "comment3"]
    },
    {
    title: "My daughter misses this swingset at Lincoln Park:(",
    thumbnail : "https://thumbs.dreamstime.com/x/child-broken-swing-6569873.jpg",
    ups: "59",
    id: "abc2",
    created_utc: 1479583399,
    comments: ["comment1", "comment2", "comment3"]
    },
    {
    title: "Thank God, they're alive! This turn is so dangerous",
    thumbnail : "http://i.dailymail.co.uk/i/pix/2015/08/20/23/2B88C0C200000578-3205065-image-a-28_1440108029352.jpg",
    ups: "76",
    id: "abc3",
    created_utc: 1479583399,
    comments: ["comment1", "comment2", "comment3"]
    },
    {
    title: "Be careful out there guys, Market street is flooding",
    thumbnail : "http://media.nbcdfw.com/images/1200*675/fw-flooding.JPG",
    ups: "187",
    id: "abc4",
    created_utc: 1479573624,
    comments: ["comment1", "comment2", "comment3"]
    },
    {
    title: "Last night's storm took out some powerlines. Did anyone lose power?",
    thumbnail : "http://stateimpact.npr.org/pennsylvania/files/2012/10/sandy-power-2-620x413.jpg",
    ups: "13",
    id: "abc5",
    created_utc: 1479573624,
    comments: ["comment1", "comment2", "comment3"]
    },
    {
    title: "Stop sign knocked over at corner of Griffin and Corbin st!!",
    thumbnail : "url",
    ups: "12",
    id: "abc06",
    created_utc: 1479573624,
    comments: ["comment1", "comment2", "comment3"]
    }];

  $scope.comments = [
    {text: "DogeLover9: Oh my god he is so cute!"},
    {text: "Observant1: Yea i saw him yesterday as well"},
    {text: "Janet6: caught my son feeding him the other night"}, 
    {text: "MickaMicka: Used to own one just like him"}];

  $scope.upvote = function () {
    $scope.stories[0].ups = parseInt($scope.stories[0].ups) + 1;
    $scope.pushIssueToBitBucket();
  };

  // $http.get('https://www.reddit.com/r/Dallas/.json')
  //   .success(function(resp){
  //     angular.forEach(resp.data.children, function(child){
  //       //console.log(child.data);
  //       $scope.stories.push(child.data);
  //     });
  //   });

  // function loadStories(params, callback) {
  //     $http.get('https://www.reddit.com/r/Dallas/new/.json', {params:params})
  //     .success(function(resp){
  //       var stories = [];
  //       angular.forEach(resp.data.children, function(child){
  //         console.log(child.data);
  //         stories.push(child.data);
  //       });
  //       $scope.getEmotionFromWatson(stories[0].title, stories[0].id);
  //       callback(stories);
  //     });
  // }

  $scope.pushIssueToBitBucket = function() {
    var req = {
      method: 'POST',
      url: 'https://api.bitbucket.org/1.0/repositories/KhaiHa/dallasapp/issues',
      headers: {
        'Authorization': 'Basic amlheGluZ3VvOkpnNTYzMzU3IQ==',
        'Content-type': 'text/plain'
      },
      data: "title=Missing doge alert"
    };

    $http(req).then(function(){
      console.log("Good Push")
    }, function(){
      console.log("Bad Push")
    });
  };

  $scope.getEmotionFromWatson = function(stringToAnalyze, id) {
    var req = {
      method: 'POST',
      url: 'https://watson-api-explorer.mybluemix.net/tone-analyzer/api/v3/tone?version=2016-05-19&tones=emotion&username=de94ebd9-b4bc-4e0f-930f-db1c32339b12&password=ACAitaYTM7OH',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      data: {
        "text": stringToAnalyze
      }
    };

    // if(watsonRequests < 8) {
      $http(req).then(function (response) {
        angular.forEach($scope.stories, function (story) {
            var tones = response.data.document_tone.tone_categories[0].tones;
            var winner = {};
            winner.score = -1;
            angular.forEach(tones, function (tone) {
              if (tone.score > winner.score) {
                winner.score = tone.score;
                winner.face = tone.tone_name;
              }
            });
            story.showAngerFace = false;
            story.showJoyFace = false;
            story.showSadFace = false;
            story.showDisgustFace = false;
            story.showFearFace = false;

            switch (winner.face) {
              case 'Anger':
                story.showAngerFace = true;
                break;
              case 'Disgust':
                story.showDisgustFace = true;
                break;
              case 'Fear':
                story.showFearFace = true;
                break;
              case 'Joy':
                story.showJoyFace = true;
                break;
              case 'Sadness':
                story.showSadFace = true;
                break;
            }
        });

      }, function () {
        console.log("Bad Tone Call");
       
      });
    
  };

  $scope.getEmotionFromWatson($scope.stories[0].title, $scope.stories[0].id);

  // $scope.loadOlderStories = function(){
  //   var params = {}
  //   if($scope.stories.length > 0){
  //     params['after'] = $scope.stories[$scope.stories.length -1].name
  //   }
  //   loadStories(params, function(olderStories){
  //     $scope.stories = $scope.stories.concat(olderStories);
  //     $scope.$broadcast('scroll.infiniteScrollComplete');
  //   });
  //   // $http.get('https://www.reddit.com/r/Dallas/.json', {params:params})
  //   //   .success(function(resp){
  //   //     angular.forEach(resp.data.children, function(child){
  //   //       //console.log(child.data);
  //   //       $scope.stories.push(child.data);
  //   //     });
  //   //     $scope.$broadcast('scroll.infiniteScrollComplete');
  //   //   });
  //   }

  //   $scope.loadNewerStories = function(){
  //     var params = {'before': $scope.stories[0].name};

  //     loadStories(params, function(newerStories){
  //       $scope.stories = newerStories.concat($scope.stories);
  //       $scope.$broadcast('scroll.refreshComplete');
  //     });
  //   }

});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

}());
